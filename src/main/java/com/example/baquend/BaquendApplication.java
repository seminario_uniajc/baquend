package com.example.baquend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaquendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaquendApplication.class, args);
	}

}
